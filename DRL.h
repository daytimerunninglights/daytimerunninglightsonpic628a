#include <16F628A.h>
#device *=16
# FUSES WDT                    //No Watch Dog Timer
# FUSES HS                       
//High speed Osc (> 4mhz for PCM/PCH) (>10mhz for PCD)
# FUSES PUT                      //No Power Up Timer
# FUSES NOPROTECT                //Code not protected from reading
# FUSES BROWNOUT                 //No brownout reset
# FUSES MCLR                   //Master Clear pin used for I/O
# FUSES NOLVP                    //No low voltage prgming
# FUSES NOCPD                    //No EE protection
# use delay(clock=20000000)


