#include <DRL.h>

//-----------------------------------------------------------------//
//            ������ ����������                            
//-----------------------------------------------------------------//


const int8 time_to_start = 70;
const int8 time_to_delay_lamp = 30;

int8 pwm_level = 76;

int8 drl_state = 0;
int8 pwm_vol = 0;

int8 oil_count = time_to_start;
int8 lamp_count = 0;

int1 oil_signal = 1;
int1 lamp_signal = 0;
int1 time_dcs = 0;


//-----------------------------------------------------------------//
//            ������ �������                                      //
//-------------------------------------------------------------------------

#int_TIMER1
void TIMER1_isr(void)
{
	time_dcs = 1;
	restart_wdt();
}

//-------------------------------------------------------------------------

void signalCheck()
{
	if (oil_signal == 1 || lamp_signal == 1) drl_state = 2;
	else drl_state = 1;
}

void checkLampState(int1 l_s)
{
	if (lamp_count == time_to_delay_lamp && l_s == 1) 
	{	lamp_signal = 1;
		signalCheck(); };

	if (lamp_count >= time_to_delay_lamp && l_s == 0) 
	{ 	lamp_signal = 0;
		lamp_count = 0;
		signalCheck(); };
	
	if (lamp_count < time_to_delay_lamp && lamp_count > 2 && l_s == 0) 
	{ 	lamp_count = 0;
		pwm_level = pwm_level + 12;
		if ( pwm_level > 210) pwm_level = 20;
		write_eeprom(0,pwm_level);   
		signalCheck();
	};
}

void drlCheckState()
{
switch (drl_state)
	{
		case 0: break;

		case 1: if (pwm_level == pwm_vol) drl_state = 0;
				else {
					if (pwm_level > pwm_vol) pwm_vol ++;
					if (pwm_level < pwm_vol) pwm_vol = pwm_level;
					set_pwm1_duty(pwm_vol);
				};
				break;

		case 2: if (pwm_vol == 0) drl_state = 0;
				else {
					pwm_vol --;
					set_pwm1_duty(pwm_vol);
				};
				break;
	}


			if (input_state(pin_B0) == 1) oil_count = time_to_start;
			else if (oil_count > 0) oil_count --;


	if (oil_count == 0 && oil_signal == 1) 
	{ oil_signal = 0; 
	  signalCheck(); 
	};

	if (oil_count > 0 && oil_signal == 0) 
	{ oil_signal = 1;
	  signalCheck(); 
	};

	if (input_state(pin_B1) == 1 && lamp_count < 210) lamp_count ++;
	if (lamp_count == time_to_delay_lamp) checkLampState(1);
	if (input_state(pin_B1) == 0 && lamp_count > 0) checkLampState(0); 


	time_dcs = 0;

}

void init()
{

  setup_timer_0(RTCC_INTERNAL | RTCC_DIV_1);
  setup_timer_1(T1_INTERNAL | T1_DIV_BY_4);
  setup_timer_2(T2_DIV_BY_16,255,1);
  setup_comparator(NC_NC_NC_NC);
  setup_vref(FALSE);

  set_tris_b(0b00000011);
  set_tris_A(0b00000000);
  enable_interrupts(INT_TIMER1);
  enable_interrupts(GLOBAL);
  setup_ccp1(CCP_PWM);
  setup_wdt(WDT_2304MS);
  set_pwm1_duty(0);
}


//-------------------------------------------------------------------------

void main()
{
	init();
	drl_state = 0;
	pwm_level=read_eeprom(0); 
  	while (true)
  	{
		if (time_dcs == 1) drlCheckState();
  	}
}

